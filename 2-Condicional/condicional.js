'use strict';

/*A continuación, utiliza un condicional para sacar por consola la frase correspondiente de la siguientes:

- A _nombre_ le corresponde el descuento infantil (menores de 12).
- A _nombre_ le corresponde el descuento juvenil (menores de 30).
- A _nombre_ le corresponde el descuento de jubilados (mayores de 60).
- A _nombre_ no le corresponde ningún descuento.
 */
const nombre = "cecilia";
const age = 33;
const color = "azul"

console.log(`Hola me llamo ${nombre}, tengo ${age} años, y mi color favolito es el ${color}`);

const edad = +prompt("Introduce tu edad: ");


if (edad <= 12) {
console.log(`A ${nombre} le corresponde el descuento infantil (menores de 12).`);
} else if (edad <=30 ) {
console.log(`A ${nombre} le corresponde descuento juvenil (menores de 30).`);
} else if (edad > 60) {
console.log(`A ${nombre} le corresponde descuento de jubilados (mayores de 60).`);
} else {
  console.log(`A ${nombre} no le corresponde ningún descuento.`);
}

